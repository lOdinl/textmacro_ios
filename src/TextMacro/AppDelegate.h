//
//  AppDelegate.h
//  TextMacro
//
//  Created by Leon Pham on 5/31/14.
//  Copyright (c) 2014 Leon Pham. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
